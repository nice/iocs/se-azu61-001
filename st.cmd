# ---
require essioc
require azurap61l

epicsEnvSet("IPADDR", "172.30.32.19")
epicsEnvSet("IPPORT", "10001")
epicsEnvSet("LOCATION", "SE: $(IPADDR):$(IPPORT)")
epicsEnvSet("SYS", "SE-SEE")
epicsEnvSet("DEV", "SE-AZU61-001")
epicsEnvSet("PORTNAME", "$(SYS)-$(DEV)")
epicsEnvSet("P", "$(SYS):")
epicsEnvSet("R", "$(DEV):")
epicsEnvSet("A", -1)

iocshLoad("$(essioc_DIR)/common_config.iocsh")
iocshLoad("$(azurap61l_DIR)/azurap61l.iocsh", "P=$(P), R=$(R), IPADDR=$(IPADDR), IPPORT=$(IPPORT), PORTNAME=$(PORTNAME)")
# ...
